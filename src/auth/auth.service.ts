import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import * as bcrypt from 'bcrypt';
import { UsersService } from '../users/users.service';

@Injectable()
export class AuthService {
    constructor(
        private userService: UsersService,
        private jwtService: JwtService,
    ) {}

    async validateUser(username: string, pass: string): Promise<any> {
        const user = await this.userService.findOne(username);
        const isMatch = await bcrypt.compare(pass, user.password);
        if (user && isMatch) {
            const { password, ...result } = user;
            return result;
        }
        return null;
    }

    async login(user: any): Promise<any> {
        const payload = {
            _id: user._doc._id.toString(),
            username: user._doc.username,
        };
        const token = this.jwtService.sign(payload);
        return {
            authToken: token,
        };
    }
}
