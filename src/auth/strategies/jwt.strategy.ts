import { ExtractJwt, Strategy } from 'passport-jwt';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable } from '@nestjs/common';
import { jwtConstants } from '../constants';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
    constructor() {
        super({
            jwtFromRequest: ExtractJwt.fromAuthHeaderWithScheme('Bearer'),
            ignoreExpiration: false,
            secretOrKey: jwtConstants.secretKey,
            algorithm: ['RS256'],
        });
    }

    async validate(payload: any): Promise<any> {
        return { _id: payload._id, username: payload.username };
    }
}
