import { Module } from '@nestjs/common';
import { UsersModule } from './users/users.module';
import { MongooseModule } from '@nestjs/mongoose';
import { AuthModule } from './auth/auth.module';
import { ConfigModule } from '@nestjs/config';
import { AppController } from './app.controller';

@Module({
    imports: [
        MongooseModule.forRoot('mongodb://localhost/assetmanagement'),
        ConfigModule.forRoot(),
        UsersModule,
        AuthModule,
    ],
    controllers: [AppController],
})
export class AppModule {}
