/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable prettier/prettier */
import {
    Controller,
    Get,
    Post,
    Delete,
    Body,
    Param,
    Patch,
    Request,
    UseGuards,
} from '@nestjs/common';
import { User } from '../users/schema/users.schema';
import { UsersService } from '../users/users.service';
import { CreateUserDto } from '../users/dtos/create-users.dto';
import { UpdateUserDto } from '../users/dtos/update-users.dto';
import { AuthService } from '../auth/auth.service';
import { JwtAuthGuard } from '../auth/guards/jwt-auth.guard';
import { LocalAuthGuard } from '../auth/guards/local-auth.guard';

@Controller('users')
export class UsersController {
    constructor(
        private authService: AuthService,
        private usersService: UsersService,
    ) {}
    @Post()
    async create(@Body() createUserDto: CreateUserDto) {
        await this.usersService.create(createUserDto);
    }
    @UseGuards(JwtAuthGuard)
    @Get('/:email')
    async findByEmail(@Param('email') email: string): Promise<User> {
        return this.usersService.findOne(email);
    }

    @Get('/:id')
    async findOne(@Param('id') id: string): Promise<User> {
        return this.usersService.findOne(id);
    }

    @UseGuards(LocalAuthGuard)
    @Post('login')
    async loginUser(@Request() req: any): Promise<any> {
        return this.authService.login(req.user);
    }

    @Get()
    async findAll(): Promise<User[]> {
        return this.usersService.findAll();
    }

    //update user
    @Patch(':id')
    async update(@Param('id') id: string, @Body() body: UpdateUserDto) {
        return this.usersService.update(id, body);
    }

    @Delete(':id')
    async delete(@Param('id') id: string) {
        return this.usersService.delete(id);
    }
}
