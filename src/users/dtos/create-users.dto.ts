import { IsString, IsNumber, IsEmail, IsInt } from 'class-validator';
import { Length, Min, IsOptional, IsNotEmpty } from 'class-validator';

export class CreateUserDto {
    @IsString()
    @Length(6, 255)
    @IsNotEmpty()
    name: string;

    @IsNumber()
    @IsInt()
    @Min(0)
    @IsOptional()
    age: number;

    @IsString()
    @Length(6, 11)
    @IsOptional()
    phone: string;

    @IsString()
    @IsEmail()
    @Length(6, 255)
    @IsNotEmpty()
    email: string;

    @IsString()
    @Length(6, 255)
    @IsNotEmpty()
    username: string;

    @IsString()
    @Length(6, 255)
    @IsNotEmpty()
    password: string;
}
