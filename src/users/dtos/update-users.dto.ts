import { IsString, IsNumber, IsInt } from 'class-validator';
import { Length, Min, IsOptional } from 'class-validator';

export class UpdateUserDto {
    @IsString()
    @Length(6, 255)
    @IsOptional()
    name: string;

    @IsNumber()
    @IsInt()
    @Min(0)
    @IsOptional()
    age: number;

    @IsString()
    @Length(6, 11)
    @IsOptional()
    phone: string;

    @IsString()
    @Length(6, 255)
    @IsOptional()
    password: string;
}
