/* eslint-disable @typescript-eslint/no-this-alias */
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import mongoose from 'mongoose';
import * as bcrypt from 'bcrypt';

export type UserDocument = User & mongoose.Document;

@Schema({ timestamps: true })
export class User {
    @Prop({ type: String, trim: true, required: true })
    name: string;

    @Prop({ type: String, trim: true, required: true })
    email: string;

    @Prop({ type: String, required: true })
    password: string;

    @Prop({ type: Number, default: undefined })
    age: number;

    @Prop({ type: String, trim: true, default: undefined })
    phone: string;

    @Prop({ type: String, trim: true, required: true })
    username: string;

    @Prop({ type: String, default: undefined })
    avatar: string;
}

export const UserSchema = SchemaFactory.createForClass(User);

UserSchema.pre('save', async function (next) {
    const user = this;
    if (user.isModified('password')) {
        const salt = await bcrypt.genSalt(10);
        user.password = await bcrypt.hash(user.password, salt);
    }
    next();
});
