import {
    ForbiddenException,
    HttpException,
    HttpStatus,
    Injectable,
    UnprocessableEntityException,
} from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { User, UserDocument } from './schema/users.schema';
import { CreateUserDto, UpdateUserDto } from './dtos/users.dto';
import { use } from 'passport';

@Injectable()
export class UsersService {
    constructor(
        @InjectModel(User.name) private userModel: Model<UserDocument>,
    ) {}

    async findAll(): Promise<User[]> {
        return this.userModel.find().exec();
    }

    async findOne(username: string): Promise<User> {
        const user = await this.userModel.findOne({ username });
        return user;
    }

    async create(
        createUserDto: CreateUserDto,
    ): Promise<{ message: string; result: User }> {
        const user = new this.userModel(createUserDto);
        const { username, email, password } = createUserDto;

        const isExistUser = await this.userModel.findOne({ username, email });

        if (!isExistUser) {
            await user.save();
            return { message: 'Created', result: user };
        }
        throw new HttpException('User already Exists', HttpStatus.BAD_REQUEST);
    }
    async update(id: string, data: UpdateUserDto) {
        const user = await this.userModel.findByIdAndUpdate({ _id: id }, data, {
            new: true,
        });
        if (!user) {
            throw new Error('User not found!');
        }
        return user;
    }

    async delete(id: string) {
        const deletedUser = await this.userModel
            .findByIdAndRemove({ _id: id })
            .exec();
        return deletedUser;
    }
}
